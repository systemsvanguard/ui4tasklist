import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomateddetailComponent } from './automateddetail.component';

describe('AutomateddetailComponent', () => {
  let component: AutomateddetailComponent;
  let fixture: ComponentFixture<AutomateddetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomateddetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomateddetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
