import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedlistComponent } from './automatedlist.component';

describe('AutomatedlistComponent', () => {
  let component: AutomatedlistComponent;
  let fixture: ComponentFixture<AutomatedlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomatedlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
