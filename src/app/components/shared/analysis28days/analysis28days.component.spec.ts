import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Analysis28daysComponent } from './analysis28days.component';

describe('Analysis28daysComponent', () => {
  let component: Analysis28daysComponent;
  let fixture: ComponentFixture<Analysis28daysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Analysis28daysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Analysis28daysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
