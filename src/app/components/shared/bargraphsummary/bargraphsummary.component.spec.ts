import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BargraphsummaryComponent } from './bargraphsummary.component';

describe('BargraphsummaryComponent', () => {
  let component: BargraphsummaryComponent;
  let fixture: ComponentFixture<BargraphsummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BargraphsummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BargraphsummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
