import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
// pages & components
import { NotfoundComponent } from './components/pages/notfound/notfound.component';
import { SitenavbarComponent } from './components/shared/sitenavbar/sitenavbar.component';
import { SitefooterComponent } from './components/shared/sitefooter/sitefooter.component';
import { EngineerlistComponent } from './components/admin/engineers/engineerlist/engineerlist.component';
import { EngineeraddComponent } from './components/admin/engineers/engineeradd/engineeradd.component';
import { EngineerdetailComponent } from './components/admin/engineers/engineerdetail/engineerdetail.component';
import { TasklistComponent } from './components/admin/admintasks/tasklist/tasklist.component';
import { TaskaddComponent } from './components/admin/admintasks/taskadd/taskadd.component';
import { TaskdetailComponent } from './components/admin/admintasks/taskdetail/taskdetail.component';
import { WeekendComponent } from './components/admin/weekend/weekend.component';
import { SettingsComponent } from './components/admin/settings/settings.component';
import { DelegatesComponent } from './components/admin/delegates/delegates.component';
import { AboutComponent } from './components/pages/about/about.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { LoginComponent } from './components/pages/login/login.component';
import { Analysis28daysComponent } from './components/shared/analysis28days/analysis28days.component';
import { BargraphsummaryComponent } from './components/shared/bargraphsummary/bargraphsummary.component';
import { Tester1Component } from './components/pages/tester1/tester1.component';
import { Tester2Component } from './components/pages/tester2/tester2.component';
import { AutomatedlistComponent } from './components/main/automated/automatedlist/automatedlist.component';
import { AutomateddetailComponent } from './components/main/automated/automateddetail/automateddetail.component';
import { ManuallistComponent } from './components/main/manual/manuallist/manuallist.component';
import { ActivityComponent } from './components/main/activity/activity.component';
import { ResourcesComponent } from './components/main/resources/resources.component';
import { HomeComponent } from './components/main/home/home.component';
import { WeightsComponent } from './components/admin/weights/weights.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    EngineerlistComponent,
    EngineeraddComponent,
    EngineerdetailComponent,
    TasklistComponent,
    TaskaddComponent,
    TaskdetailComponent,
    WeekendComponent,
    SettingsComponent,
    DelegatesComponent,
    AboutComponent,
    NotfoundComponent,
    ContactComponent,
    LoginComponent,
    SitenavbarComponent,
    SitefooterComponent,
    Analysis28daysComponent,
    BargraphsummaryComponent,
    Tester1Component,
    Tester2Component,
    AutomatedlistComponent,
    AutomateddetailComponent,
    ManuallistComponent,
    ActivityComponent,
    ResourcesComponent,
    HomeComponent,
    WeightsComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
